/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 **/
import { Server } from 'hapi';
import {getStock} from "./requestUtils";

const init = async () => {
  const server = new Server({
    port: 3333,
    host: 'localhost'
  });


  server.method('getStock', getStock, {
    cache: {
      expiresIn: 60000,
      generateTimeout: 10000,
    },
    generateKey: (req) => {
      return req.symbol+req.period;
    }
  });


  server.route({
    method: 'POST',
    path: '/api/GetStock',
    handler: async (request, h) => {
      return new Promise(async (resolve, reject) => {
        await server.methods.getStock(request.payload, (data) => {
          if (data.error) {
            return reject(data);
          }
           resolve(data);
        });
      });
    }
  });


  await server.start();
  console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', err => {
  console.log(err);
  process.exit(1);
});

init();
