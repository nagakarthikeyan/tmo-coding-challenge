import {request} from "https";

export const getStock = async (payload, callback) => {
  const {apiURL, symbol, period, apiKey} = payload;
  let completeData = '';
  await request(`${apiURL}/beta/stock/${symbol}/chart/${period}?token=${apiKey}`,
    (response) => {
      response.on('data', (partial) => {
        completeData += partial;
      });
      response.on('end', () => {
        return callback(completeData);
      });
      response.on('error', (error) => {
        return callback({error: error});
      });
    }).end();
};
