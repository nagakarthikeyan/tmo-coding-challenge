import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import {
  StocksAppConfig,
  StocksAppConfigToken
} from '@coding-challenge/stocks/data-access-app-config';
import { Effect } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';
import { map } from 'rxjs/operators';
import {
  FetchPriceQuery,
  PriceQueryActionTypes,
  PriceQueryFetched,
  PriceQueryFetchError
} from './price-query.actions';
import { PriceQueryPartialState } from './price-query.reducer';
import { PriceQueryResponse } from './price-query.type';
import * as _ from "lodash";

@Injectable()
export class PriceQueryEffects {
  @Effect() loadPriceQuery$ = this.dataPersistence.fetch(
    PriceQueryActionTypes.FetchPriceQuery,
    {
      run: (action: FetchPriceQuery, state: PriceQueryPartialState) => {
        return this.httpClient
          .post<PriceQueryResponse[]>(
            `/api/GetStock`, {
              apiURL: this.env.apiURL,
              symbol: action.symbol,
              period: action.period,
              apiKey: this.env.apiKey
            }
          )
          .pipe(
            map(resp => {
              if(action.fromDate && action.toDate) {
                const filteredResponse = _.filter(resp, function (stock) {
                  const stockDate = new Date(stock.date);
                  return stockDate.getTime() >= action.fromDate.getTime() &&
                    stockDate.getTime() <= action.toDate.getTime()
                });
                return new PriceQueryFetched(filteredResponse as PriceQueryResponse[])
              }
              else {
                return new PriceQueryFetched(resp as PriceQueryResponse[])
              }
            })
          );
      },

      onError: (action: FetchPriceQuery, error) => {
        return new PriceQueryFetchError(error);
      }
    }
  );

  constructor(
    @Inject(StocksAppConfigToken) private env: StocksAppConfig,
    private httpClient: HttpClient,
    private dataPersistence: DataPersistence<PriceQueryPartialState>
  ) {}
}
