import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StocksComponent } from './stocks.component';
import {MatButtonModule, MatFormFieldModule, MatInputModule, MatSelectModule,  MatDatepickerModule,
  MatNativeDateModule} from "@angular/material";
import {SharedUiChartModule} from "@coding-challenge/shared/ui/chart";
import {ReactiveFormsModule} from "@angular/forms";
import {PriceQueryFacade} from "@coding-challenge/stocks/data-access-price-query";
import {of} from "rxjs";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";

describe('StocksComponent', () => {
  let component: StocksComponent;
  let fixture: ComponentFixture<StocksComponent>;
  let priceQueryStub: any;
  beforeEach(async(() => {

    priceQueryStub = {
      priceQueries$: of({data: 'sample'})
    };

    TestBed.configureTestingModule({
      imports:[
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatButtonModule,
        SharedUiChartModule,
        ReactiveFormsModule,
        NoopAnimationsModule,
        MatDatepickerModule,
        MatNativeDateModule
      ],
      declarations: [ StocksComponent ],
      providers:[StocksComponent,{provide: PriceQueryFacade, useValue: priceQueryStub}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
