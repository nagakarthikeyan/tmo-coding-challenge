import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';
import { Subscription } from "rxjs";

@Component({
  selector: 'coding-challenge-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit,OnDestroy {
  stockPickerForm: FormGroup;
  symbol: string;
  period: string;

  quotes$ = this.priceQuery.priceQueries$;

  timePeriods = [
    { viewValue: 'All available data', value: 'max' },
    { viewValue: 'Five years', value: '5y' },
    { viewValue: 'Two years', value: '2y' },
    { viewValue: 'One year', value: '1y' },
    { viewValue: 'Year-to-date', value: 'ytd' },
    { viewValue: 'Six months', value: '6m' },
    { viewValue: 'Three months', value: '3m' },
    { viewValue: 'One month', value: '1m' }
  ];

  currentDate: Date;

  stockPickerFormSubscription: Subscription;

  constructor(private fb: FormBuilder, private priceQuery: PriceQueryFacade) {
    this.currentDate = new Date();
    this.stockPickerForm = fb.group({
      symbol: [null, Validators.required],
      period: [null, Validators.required],
      fromDate: [null],
      toDate: [null]
    });
  }

  ngOnInit() {
    this.stockPickerFormSubscription =
      this.stockPickerForm.valueChanges.subscribe((formData)=> {
         if(formData.symbol && formData.period) {
          this.fetchQuote();
        }
      });
  }

  fetchQuote() {
    if (this.stockPickerForm.valid) {
      const {symbol, period, fromDate, toDate} = this.stockPickerForm.value;
      if (fromDate && toDate) {
        const pstDate = new Date(fromDate.getTime() - 8 * 3600 * 1000);
        if(fromDate > toDate) {
          this.stockPickerForm.setValue({fromDate: fromDate,
            toDate: fromDate, symbol: symbol, period: period});
        } else {
          this.priceQuery.fetchQuote(symbol, period, pstDate, toDate);
        }
      }
      else {
        this.priceQuery.fetchQuote(symbol, period);
      }
    }
  }

  ngOnDestroy() {
    this.stockPickerFormSubscription.unsubscribe();
  }
}
